		ADR		R3, DATA
		LDR		R1, [R3], #4
		LDR		R2, [R3]
		ADDS		R0, R1, R2
		MOV		R5, #127
		RSB		R0, R5, R0, LSR #23
		ADDCS	R0, R0, #0X200
		MOV		R5, R1, LSL #9
		MOV		R6, R2, LSL #9
		ADD		R5, R5, #1
		MOV		R5, R5, ROR #9
		MOV		R6, R6, ROR #9
		MOV		R7, R5
		MOV		R8, #1
		MOV		R9, #1
		MOV		R10, #22
MANMUL	AND		R11, R6, R9, LSL R10
		CMP		R11, #0
		ADDNE	R7, R7, R5, LSR R8
		ADD		R8, R8, #1
		SUB		R10, R10, #1
		CMP		R8, #22
		BNE		MANMUL
SHIFT	CMP		R7, R10, LSL #24
		MOVGT	R7, R7, LSR #1
		BGT		SHIFT
		MOV		R7, R7, LSL #9
		MOV		R7, R7, LSR #9
		ADD		R0, R7, R0, LSL #23
		
		
DATA		DCD		0x40400000, 0x40400000
