WAVE		STMED	SP!, {R4-R8, LR}
		ADR		R1, INWAVE
		ADR		R2, OUTWAVE
		ADD		R3, R1, #52
		MOV		R0, #0
		
WAVE_IN	LDR		R4, [R1],#4
		LDR		R5, [R1]
		LDR		R6, [R1, #4]
		ADDS		R4, R4, R5
		MOVVS	R0, #1
		ADDS		R4, R4, R6
		MOVVS	R0, #1
		CMP		R1, R3
		STRLE	R4, [R2], #4
		BLE		WAVE_IN
		STR		R4, [R2]
		LDMED	SP!, {R4-R8, PC}
		
INWAVE	DCD		500, 1000, 500, 700, 2000, 4000, 3000, 11000, 50, 300, 400, 800, 750, 300, 100000, 1000
OUTWAVE	FILL		14*4
