		ADR		R2, BITDATA0
		BL		LOOP1
		END
LOOP1	STMED	SP!, {R1-R12, LR}
		MOV		R0, #0
LOOP		LDR		R1, [R2], #4
		CMP		R1, #0
		ADDEQ	R0, R0, #32
		BEQ		LOOP
		MOV		R4, #0
LOOPCHECK	MOV		R6, #1
		AND		R5, R1, R6, LSL R4
		CMP		R5, #0
		ADDNE	R0, R0, R4
		ADD		R4, R4, #1
		BEQ		LOOPCHECK
		LDMED	SP!, {R1-R12, PC}
		
BITDATA0	DCD		0x0,0x0,0x0,0x0,0x6C000000,0x7D2059AD,0x255D2F65,0x50FAF8F5 ; 154
BITDATA1	DCD		0x0,0x0,0x0,0x0,0x0,0x0,0x338E1F77,0x12CB29B5 ; 192
BITDATA2	DCD		0x0,0x0,0x7E400000,0xF29B3C84,0x8B150CB4,0x95C73D8B,0x69D50DEF,0x7FFF1AF8 ; 86
BITDATA3	DCD		0x0,0x0,0x0,0x0,0x96800000,0xA9AA01BB,0xBBF4750A,0xE793FD94 ; 151
BITDATA4	DCD		0x0,0x0,0x0,0x0,0x0,0x80000000,0x54EA0E6D,0xA4B35210 ; 191
BITDATA5	DCD		0x92CC6BC0,0xFAB2C2C5,0xB18DB425,0xA3E1B33F,0x440AC89F,0x23E92E6E,0xEC5757BD,0x8D55121C ; 6
BITDATA6	DCD		0x0,0x0,0xC0000000,0xEA500CA0,0x48ED5256,0x979EC508,0xA0188570,0x48CAD0EF ; 94
BITDATA7	DCD		0x0,0xBC000000,0x93F01E38,0xD23E235B,0xC6C42D4B,0x7F4D4C4E,0x840049C,0x3DD55F38 ; 58
BITDATA8	DCD		0x134E7BF8,0x39BCF7D6,0xB1C5706F,0x99EEEF1D,0x83C92DCF,0xFB3FF7AF,0x8228CAEB,0xA3A04D4A ; 3
BITDATA9	DCD		0x0,0x0,0x0,0x9C000000,0x96CFF393,0xC5B753DB,0x41CC15E8,0x7EF19149 ; 122
BITDATA10	DCD		0x0,0x0,0x0,0x0,0x8A1E3A20,0x4E2510F7,0xFDE0EE37,0x941D46A2 ; 133
BITDATA11	DCD		0x8001549,0x5DCC4A74,0xEB7F75D3,0x49573B6F,0xEB292727,0xB9437664,0x66CE387C,0xAB94F006 ; 0
BITDATA12	DCD		0x0,0x0,0x67B00000,0x6EDE964C,0xE4B073B2,0xEA318770,0x42E1A16D,0xD12CFC36 ; 84
BITDATA13	DCD		0x0,0x2BC3A280,0xA4402BCC,0x664ED28B,0x21CA4429,0x390F0D2C,0xEB7D116E,0xC3E81022 ; 39
BITDATA14	DCD		0xC0000000,0xBBC369A2,0xAD97E448,0x26F3050F,0x320B762E,0xD4D92D8E,0xF74BB0C9,0xA648EF65 ; 30
BITDATA15	DCD		0x0,0x6E0F4000,0x8D200C3C,0xD190434A,0xE8798440,0x1EAC4C5F,0xCBDF21D4,0x8A5DE79D ; 46
BITDATA16	DCD		0x0,0x0,0x0,0x0,0x0,0x0,0xE1C7100,0x3E0675BE ; 200
BITDATA17	DCD		0x0,0x0,0x0,0xDA962000,0xB62A17E5,0xB46D5B03,0xCA6746AD,0x9DFD75B6 ; 109
BITDATA18	DCD		0x0,0x0,0x0,0x0,0x4655AE73,0x7BA4FAB7,0xB70B3BC0,0x870D3BB1 ; 128
BITDATA19	DCD		0x0,0x80000000,0xBA34E0CD,0x8C1502C7,0x582CCD8F,0xF0F36756,0xAB30F568,0xF85156A1 ; 63
