		MOV		R0, #8
		MOV		R1, #2
		RSB		R2, R1, #0
		MOV		R0, R0, LSL #1 ;P
		MOV		R2, R2, LSL #5 ;S
		MOV		R1, R1, LSL #5 ;A
		MOV		R4, #0
LOOP		AND		R3, R0, #3
		CMP		R3, #1
		ADDEQ	R0, R0, R1
		BLE		SKIP
		CMPGT	R3, #2
		ADDEQ	R0, R0, R2
SKIP		ASR		R0, R0, #1
		ADD		R4, R4, #1
		CMP		R4, #4
		BLT		LOOP
		ASR		R0, R0, #1
