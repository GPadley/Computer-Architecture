			;		This testbench checks MUL24X24 for correct outputs as determined by the provided data
			;		The code will run correctly even if MUL24X24 is not cooperative, as long as it does not corrupt stack with unbalanced push/pop
			;		At the end of execution R0 contains a non-negative error code, or -1 for a pass indication
PROJTB		MOV		R4, #0 ; test number
			ADR		R5, PROJTBDATA0 ; pointer to base of test data
PROJTBLOOP
			ADD		R8, R5, R4, LSL #4  ; R8 points to current test data
			LDMIA	R8, {R0,R1,R6} ; R0,R1 are test inputs. R6,R7 will be matched with outputs
			MOV		R12, R13 ; copy of R13 to save
			STMED	R13!, {R4,R5,R6,R12} ; save registers in case tested code is not cooperative
			BL		IEEE754MULT ; do multiplication
			LDMED	R13!, {R4,R5,R6,R12} ; restore registers in case tested code is not cooperative
			MOV		R11, R13 ; cannot directly compare R13
			CMP		R12, R11 ; if R12 <> R13 then MUL24X24 has corrupted the stack with unbalanced PUSH/POP
			BNE		PROJTBFAIL2
			CMP		R2, R6
			BNE		PROJTBFAIL
			ADD		R4, R4, #1
			CMP		R4, #PROJTBNUMTESTS
			BNE		PROJTBLOOP
			MOV		R0, #-1
			B		PROJTBPASS
			
IEEE754MULT
			STMED	SP!, {R0, R1, R3-R11, LR}
			LSL		R2, R0, #1
			CMP		R2, #0xFF000000
			MOVHI	R2, R0
			BHI		FINISH
			LSL		R3, R1, #1
			CMPEQ	R3, #0
			MOVEQ	R3, #3
			ADDEQ	R2, R2, R3, LSL #22
			BEQ		FINISH
			CMP		R3, #0xFF000000
			CMPEQ	R2, #0
			MOVEQ	R2, #3
			ADDEQ	R2, R3, R2, LSL #22
			BEQ		FINISH
			CMP		R3, #0xFF000000
			MOVHI	R2, R1
			BHI		FINISH
			MOVEQ	R2, R3, LSR #1
			BEQ		SIGN
			CMP		R2, #0xFF000000
			MOVEQ	R2, R2, LSR #1
			BEQ		SIGN
			CMP		R2, #0
			MOVEQ	R2, #0
			BEQ		SIGN
			CMP		R3, #0
			MOVEQ	R2, #0
			BEQ		SIGN
			MOV		R2, R2, LSR #24
			CMP		R2, #0
			MOV		R4, R0, LSL #9
			ADDNE	R4, R4, #1
			MOVNE	R4, R4, ROR #9
			MOVEQ	R4, R4, ROR #8
			MOV		R3, R3, LSR #24
			CMP		R3, #0
			MOV		R5, R1, LSL #9
			ADDNE	R5, R5, #1
			MOVNE	R5, R5, ROR #9
			MOVEQ	R5, R5, ROR #8
			ADD		R2, R2, R3
			SUB		R2, R2, #127
			MOV		R6, #0
			MOV		R7, #0
			MOV		R8, #1
			MOV		R9, #0
			MOV		R10, #32
			
MANMUL		LSLS		R11, R5, R10	;CHECK FOR OVER FLOW
			BCC		NOMUL
			ADDS		R6, R6, R4, LSL R9
			ADC		R7, R7, R4, LSR R10
NOMUL		ADD		R9, R9, #1
			RSB		R10, R9, #32
			CMP		R9, #25
			BLE		MANMUL
			MOV		R3, #9
MANT			RSB		R4, R3, #32
			SUB		R5, R4, #2
			MOV		R10, #0b111
			MOV		R11, R7, LSL R3
			ADD		R11, R11, R6, LSR R4	;24 BIT ANSWER
			AND		R10, R10, R6, LSR R5 ;CONTAINS DROPPED OFF BITS
SHIFT		CMP		R11, R8, LSL #24
			ANDGE	R9, R8, R11
			LSLGE	R10, R10, #1
			ADDGE	R10, R10, R9, LSL #2
			ADDGE	R2, R2, #1
			MOVGE	R11, R11, LSR #1
			BGT		SHIFT
			CMPLT	R11, R8, LSL #23
			ADDLT	R3, R3, #1
			SUBLT	R2, R2, #1
			BLT		MANT
			CMP		R2, #0xFF
			MOVGT	R2, #0xFF
			LSLGT	R2, R2, #23
			BGT		SIGN
			CMP		R2, #0
			MOVLT	R2, #0
			BLT		SIGN
			MOV		R11, R11, LSL #9
			MOV		R11, R11, LSR #9
			ADD		R2, R11, R2, LSL #23
			CMP		R10, #0b100
			BLT		SIGN
			ADDGT	R2, R2, #1
			BGT		SIGN
			LSLS		R3, R2, #32
			ADC		R2, R2, #0
SIGN
			TEQ		R0, R1
			ADDMI	R2, R2, #0x80000000
FINISH		LDMED	SP!, {R0, R1, R3-R11, PC}
			
PROJTBFAIL	MOV		R0, R4 ; R0 contains the number of the test that failed (0 - NUMTESTS-1)
			END
			
PROJTBFAIL2	MOV		R0, #100 ; 100 indicates that tested code has corrupted the stack pointer
			END
			
PROJTBPASS	MOV		R0, #-1 ; -1 in R0 is a PASS indication
			END
			
PROJTBNUMTESTS	EQU		21 ; constant equal to the number of tests provided. The test data must exist for each test.
			
PROJTBDATA0	DCD		0x4c57449c, 0x491a5685, 0x5601C807, 0
PROJTBDATA1	DCD		0x4c57449c, 0x00400000, 0x0CD7449C, 0
PROJTBDATA2	DCD		0x4f17c2f2, 0x4caaf006, 0x5C4AAB7E, 0
PROJTBDATA3	DCD		0x4b846afe, 0x4ceb79a3, 0x58F39A44, 0
PROJTBDATA4	DCD		0x00400000, 0x00400000, 0x00000000, 0
PROJTBDATA5	DCD		0x00000000, 0x7F800000, 0XFFC00000, 0
PROJTBDATA6	DCD		0xFFFFFFFF, 0xFFFFFCCC, 0xFFFFFFFF, 0
PROJTBDATA7	DCD		0xFFFFFCCC, 0xFFFFFFFF, 0xFFFFFCCC, 0
PROJTBDATA8	DCD		0xFC434000, 0x54535A80, 0XFF800000, 0
PROJTBDATA9	DCD		0x7F800000, 0xfF800000, 0xfF800000, 0
PROJTBDATA10	DCD		0x60100000, 0x5F800000, 0x7F800000, 0
PROJTBDATA11	DCD		0x40800000, 0x40800000, 0x41800000, 0
PROJTBDATA12	DCD		0x7F800000, 0x54535A80, 0x7F800000, 0
PROJTBDATA13	DCD		0x007FFFFF, 0x007FFFFF, 0x00000000, 0
PROJTBDATA14	DCD		0x3DCCCCCD, 0x3DCCCCCD, 0x3C23D70B, 0
PROJTBDATA15	DCD		0x21B61B34, 0x625629D4, 0x4498587F, 0
PROJTBDATA16	DCD		0x00000001, 0xFF7FFFFF, 0xB4FFFFFF, 0
PROJTBDATA17	DCD		0x007FFFFF, 0x7E800001, 0x3F800000, 0
PROJTBDATA18	DCD		0x0078F2AD, 0x2A9D2E9F, 0x00000000, 0
PROJTBDATA19	DCD		0x0078F2AD, 0x6A9D2E9F, 0x2B9485B6, 0
PROJTBDATA20	DCD		0x0078F2AD, 0x7E9D2E9F, 0x3F9485B6, 0
DATA21		DCD		0x00400000, 0x80400000, 0x80000000, 0
DATA22		DCD		0x4c8a0695, 0x43a20000
			
			;------------------------------------------------------------------------------------------------
			;		Do not change code above this line
			;		User code to be tested (the MUL24X24 subroutine and all its code) must be appended below this line
			;------------------------------------------------------------------------------------------------
			
