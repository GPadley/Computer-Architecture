			;		This testbench checks MUL24X24 for correct outputs as determined by the provided data
			;		The code will run correctly even if MUL24X24 is not cooperative, as long as it does not corrupt stack with unbalanced push/pop
			;		At the end of execution R0 contains a non-negative error code, or -1 for a pass indication
PROJTB		MOV		R4, #0 ; test number
			ADR		R5, PROJTBDATA0 ; pointer to base of test data
PROJTBLOOP
			ADD		R8, R5, R4, LSL #4  ; R8 points to current test data
			LDMIA	R8, {R0,R1,R6} ; R0,R1 are test inputs. R6,R7 will be matched with outputs
			MOV		R12, R13 ; copy of R13 to save
			STMED	R13!, {R4,R5,R6,R12} ; save registers in case tested code is not cooperative
			BL		IEEE754MULT ; do multiplication
			LDMED	R13!, {R4,R5,R6,R12} ; restore registers in case tested code is not cooperative
			MOV		R11, R13 ; cannot directly compare R13
			CMP		R12, R11 ; if R12 <> R13 then MUL24X24 has corrupted the stack with unbalanced PUSH/POP
			BNE		PROJTBFAIL2
			CMP		R2, R6
			BNE		PROJTBFAIL
			ADD		R4, R4, #1
			CMP		R4, #PROJTBNUMTESTS
			BNE		PROJTBLOOP
			MOV		R0, #-1
			B		PROJTBPASS
	
			
PROJTBFAIL	MOV		R0, R4 ; R0 contains the number of the test that failed (0 - NUMTESTS-1)
			END
			
PROJTBFAIL2	MOV		R0, #100 ; 100 indicates that tested code has corrupted the stack pointer
			END
			
PROJTBPASS	MOV		R0, #-1 ; -1 in R0 is a PASS indication
			END
			
PROJTBNUMTESTS	EQU		21 ; constant equal to the number of tests provided. The test data must exist for each test.
			
PROJTBDATA0	DCD		0x4c57449c, 0x491a5685, 0x5601C807, 0
PROJTBDATA1	DCD		0x4c57449c, 0x00400000, 0x0CD7449C, 0
PROJTBDATA2	DCD		0x4f17c2f2, 0x4caaf006, 0x5C4AAB7E, 0
PROJTBDATA3	DCD		0x4b846afe, 0x4ceb79a3, 0x58F39A44, 0
PROJTBDATA4	DCD		0x00400000, 0x00400000, 0x00000000, 0
PROJTBDATA5	DCD		0x00000000, 0x7F800000, 0XFFC00000, 0
PROJTBDATA6	DCD		0xFFFFFFFF, 0xFFFFFCCC, 0xFFFFFFFF, 0
PROJTBDATA7	DCD		0xFFFFFCCC, 0xFFFFFFFF, 0xFFFFFCCC, 0
PROJTBDATA8	DCD		0xFC434000, 0x54535A80, 0XFF800000, 0
PROJTBDATA9	DCD		0x7F800000, 0xfF800000, 0xfF800000, 0
PROJTBDATA10	DCD		0x60100000, 0x5F800000, 0x7F800000, 0
PROJTBDATA11	DCD		0x40800000, 0x40800000, 0x41800000, 0
PROJTBDATA12	DCD		0x7F800000, 0x54535A80, 0x7F800000, 0
PROJTBDATA13	DCD		0x007FFFFF, 0x007FFFFF, 0x00000000, 0
PROJTBDATA14	DCD		0x3DCCCCCD, 0x3DCCCCCD, 0x3C23D70B, 0
PROJTBDATA15	DCD		0x21B61B34, 0x625629D4, 0x4498587F, 0
PROJTBDATA16	DCD		0x00000001, 0xFF7FFFFF, 0xB4FFFFFF, 0
PROJTBDATA17	DCD		0x007FFFFF, 0x7E800001, 0x3F800000, 0
PROJTBDATA18	DCD		0x0078F2AD, 0x2A9D2E9F, 0x00000000, 0
PROJTBDATA19	DCD		0x0078F2AD, 0x6A9D2E9F, 0x2B9485B6, 0
PROJTBDATA20	DCD		0x0078F2AD, 0x7E9D2E9F, 0x3F9485B6, 0
			
			;------------------------------------------------------------------------------------------------
			;		Do not change code above this line
			;		User code to be tested (the MUL24X24 subroutine and all its code) must be appended below this line
			;------------------------------------------------------------------------------------------------
			
