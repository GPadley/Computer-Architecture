		MVN		R10, R4 ;R10 := -(R4+1)
		MOV		R2, R0  ;R2 takes the value of R0
		MVN		R0, R1  ;R0 takes the inverted value of R1 instead of inverting and then moving
		MOV		R1, #0  ;Because you know that R1 will be 0 at the end, you can use R2 as the temp storage and set R1 to 0 at the end
		MOV		R3, #0x3A